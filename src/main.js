import './assets/main.css'

import { createApp } from 'vue'
import { VueHeadMixin, createHead } from '@unhead/vue'
import { createPinia, setMapStoreSuffix } from 'pinia'
import { VueRecaptchaPlugin } from 'vue-recaptcha'

import App from './App.vue'
import router from './router'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

// Plugins
import { registerPlugins } from '@/plugins'

import './axios'

const vuetify = createVuetify({
  components,
  directives,
})

setMapStoreSuffix("")

const app = createApp(App)
  .use(vuetify)


function ConfigPiniaPlugin() {
  return {
      api: {
          url: import.meta.env.VITE_API_URL
      },
      router
  }
}
const pinia = createPinia()

pinia.use(ConfigPiniaPlugin)

app.use(pinia)

app.config.devtools = true

registerPlugins(app)

const head = createHead()
app.mixin(VueHeadMixin)
app.use(head)
app.use(VueRecaptchaPlugin, {  
  v3SiteKey: '6LeCxQ4pAAAAANl7q__08HS37EBrS_bXmMQDiiVH',
})

app.mount('#app')
