import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import DocumentsView from '../views/DocumentsView.vue'
import SitesView from '../views/SitesView.vue'
import TaxonsView from '../views/TaxonsView.vue'
import HabitatsView from '../views/HabitatsView.vue'
import SentencesView from '../views/SentencesView.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import { useDataStore } from '../stores/DataStore'
import { useFormStore } from '../stores/FormStore'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', name: 'home', component: HomeView},
    { path: '/documents', name: 'documents', component: DocumentsView},
    { path: '/sites', name: 'sites', component: SitesView},
    { path: '/taxa', name: 'taxa', component: TaxonsView},
    { path: '/habitats', name: 'habitats', component: HabitatsView},
    { path: '/sentences', name: 'sentences', component: SentencesView},
    { path: '/login', name: 'login', component: Login},
    { path: '/register', name: 'register', component: Register},
  ]
})

router.beforeEach((to, from, next) => {
  const dataStore = useDataStore()
  const formStore = useFormStore()

  const token = localStorage.getItem('token')
  if (token === null) {
    if (to.name === 'login' || to.name === 'register') {
      next();
    }
    else {
      next({name: 'login'})
    }
  } else {
    if (to.name === 'login' || to.name === 'register') {
      next({name: 'home'})
    }
    else {
      dataStore.$reset()
      formStore.$reset()
      dataStore.page = to.name
      next()
    }
  }
});

export default router