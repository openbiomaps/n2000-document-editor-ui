import { defineStore } from 'pinia'
import axios from 'axios'
import { useFormStore } from './FormStore'
import { useValidationStore } from './ValidationStore'
import Ajv from 'ajv'
import addFormats from 'ajv-formats'

export const useDataStore = defineStore('data', {
  state: () => ({
    data: [],
    selected: false,
    toolbarTitle: false,
    getDataParams: {},
    page: false,
    loading: false,
    validation: [],
    totalItems: false,

    sites: [],
    site: false,
    documents: [],
    selectedDocumentId: false,
    document: false,
    edit: false
  }),
  actions: {
    getData() {
      return new Promise((resolve, reject) => {
        this.loading = true
        axios
          .get(`/api/${this.endpoint()}`, { params: this.getDataParams })
          .then((data) => {
            this.data = data.data.data,
            this.totalItems = data.data.total,
            this.loading = false
            resolve()
          })
          .catch((err) => reject(err))
      })
    },

    // document editor specific methods

    loadDocument(id) {
      return new Promise((resolve) => {
        const formStore = useFormStore()
        const validationStore = useValidationStore()
        this.loading = true
        formStore.$reset()

        axios
          .get(`/api/documents/${id}`)
          .then((data) => {
            validationStore.data = JSON.parse(JSON.stringify(data.data))
            validationStore.validateDocument()
            data.data.jelolo_fajok = this.keysById(data.data.jelolo_fajok)
            data.data.jelolo_elohelyek = this.keysById(data.data.jelolo_elohelyek)
            this.selected = data.data
            this.loading = false
            resolve()
          })
          .catch((err) => console.log(err))
      })
    },
    keysById(arr) {
      const obj = {}
      arr.forEach((el) => {
        obj[el._id] = el
      })
      return obj
    },
    async validateDocument(data) {
      if (this.page != 'documents') {
        return
      }
      const ajv = new Ajv({ allErrors: true })
      addFormats(ajv)
      ajv.addKeyword('loadSuggestions')
      const axiosdata = await axios.get(`/api/schema/celdokumentum`)
      const schema = axiosdata.data

      const validate = ajv.compile(schema)
      const valid = validate(data)
      if (!valid) {
        this.validation = validate.errors
      }
    },
    editDocument(id) {
      this.loadDocument(id)
      this.edit = true
    },
    viewDocument(id) {
      this.loadDocument(id)
      this.edit = false
    },
    unloadDocument() {
      const formStore = useFormStore()

      this.document = false
      this.selectedDocumentId = false
      this.edit = false
      formStore.$reset()
    },
    endpoint() {
      return this.page + (this.selected ? `/${this.selected._id}` : '')
    },
    refresh() {
      if (this.page == 'documents') {
        this.getData().then(() => {
          if (this.selected) {
            this.loadDocument(this.selected._id)
          }
        })
      } else {
        this.selected = false
        this.getData()
      }
    },
    back() {
      this.data = []
      this.selected = false
      this.getData()
    },
    toolbarTitle() {
      if (this.page === 'documents') {
        console.log(this.selected);
        return this.selected
          ? `${this.selected.site.terulet_azonositoja} - ${this.selected.site.terulet_neve}`
          : 'Dokumentumok'
      } else {
        return this.page
      }
    },
    menuLevel() {
      // ha van hiválasztott dokumentum
      if (this.selectedDocumentId) {
        return 3
      }
      // ha van kiválasztott site
      else if (this.site) {
        return 2
      } else {
        return 1
      }
    }
  }
})
