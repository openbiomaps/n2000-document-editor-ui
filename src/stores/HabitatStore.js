import { defineStore } from 'pinia'
import axios from 'axios'

export const useHabitatStore = defineStore('habitat', {
  state: () => ({
    loading: false,
    habitats: [],
    selectedHabitat: {}
  }),
  actions: {
    loadHabitats() {
      return new Promise((resolve, reject) => {
        this.loading = true
        axios
          .get(`/api/habitats`)
          .then((data) => {
            this.habitats = data.data
            this.loading = false
            resolve()
          })
          .catch((err) => reject(err))
      })
    }
  }
})
