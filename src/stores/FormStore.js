import { defineStore } from 'pinia'
import { useDataStore } from './DataStore'
import { useAuthStore } from './AuthStore'
import axios from 'axios'

import { Draft07 } from 'json-schema-library'
import { get, set } from 'lodash'

import { Resolver } from '@stoplight/json-ref-resolver'
import { resolveFile, resolveHttp } from '@stoplight/json-ref-readers'


export const useFormStore = defineStore('form', {
  state: () => ({
    endpoint: '',
    pointer: null,
    form_type: null,
    schema: null,
    uischema: null,
    data: {},
    errors: [],
    loading: false,
    revisions: false,
    dataPath: false,
    value: {}
  }),
  actions: {
    loadCreateForm(name = false) {
      this.loading = true
      this.form_type = 'create'

      const dataStore = useDataStore()
      this.endpoint = name ? name + '/' : ''

      axios
        .get(`/api/${dataStore.endpoint()}/${this.endpoint}${this.form_type}`)
        .then(async (data) => {
          
          const resolver = new Resolver({
            resolvers: {
              https: { resolve: resolveHttp },
              http: {
                async resolve(ref) {
                  return await axios.get(ref).then((data) => data.data)
                }
              },
              file: { resolve: resolveFile }
            }
          })
          const dereferencedSchema = await resolver.resolve(data.data.schema)
          const jsonSchema = new Draft07(dereferencedSchema.result)
          this.schema = jsonSchema.getSchema({ pointer: '' })
          this.uischema = data.data.uischema
          this.data = data.data.data
          this.loading = false
        })
        .catch((err) => console.log(err))
    },

    loadEditForm2(name, ids = [], pointer = '') {
      this.loading = true
      const dataStore = useDataStore()
      this.form_type = 'edit'

      this.pointer = pointer

      this.dataPath = this.getDataPath(ids)

      this.endpoint = name ? name + '/' : ''
      axios
        .get(`/api/${dataStore.endpoint()}/${this.endpoint}${this.form_type}`)
        .then(async (data) => {
          const resolver = new Resolver({
            resolvers: {
              https: { resolve: resolveHttp },
              http: {
                async resolve(ref) {
                  return await axios.get(ref).then((data) => data.data)
                }
              },
              file: { resolve: resolveFile }
            }
          })
          const dereferencedSchema = await resolver.resolve(data.data.schema)
          const jsonSchema = new Draft07(dereferencedSchema.result)
          this.schema = jsonSchema.getSchema({ pointer })

          if (data.data.uischema !== undefined) {
            this.uischema = data.data.uischema  
          }
          else if (this.dataPath[this.dataPath.length - 1] != '') {
            const uischema = await axios.get(`/api/uischema/${[...this.dataPath].pop()}`);
            this.uischema = uischema.data ? uischema.data : undefined
          }
          else {
            this.uischema = undefined
          }

          this.value = !this.pointer ? data.data.data : get(data.data.data, this.dataPath)
          set(this.data, this.dataPath, this.value)

          this.loading = false
        })
        .catch((err) => console.log(err))
    },

    getDataPath(ids) {
      const dataPath = this.pointer.replace(/^\//, '').split('/')
      let ids_idx = 0
      for (let index = 0; index < dataPath.length; index++) {
        const element = dataPath[index]
        if (element === 'items') {
          dataPath[index] = ids[ids_idx]
          ids_idx++
        }
      }

      return dataPath
    },

    async loadRevisions() {
      const dataStore = useDataStore()

      this.revisions = []
      await axios
        .get(`/api/${dataStore.endpoint()}/revisions`)
        .then((data) => {
          this.revisions = data.data
          this.loading = false
        })
        .catch((err) => console.log(err))
    },

    async create() {
      const dataStore = useDataStore()
      const authStore = useAuthStore()

      await authStore.getCsrfToken()
      axios.defaults.headers.common['X-XSRF-TOKEN'] = authStore.csrfToken
      axios
        .post(`/api/${dataStore.endpoint()}/${this.endpoint}`, this.data)
        .then(() => {
          dataStore.refresh()
          this.$reset()
        })
        .catch((err) => console.log(err))
    },

    async update() {
      const dataStore = useDataStore()
      const authStore = useAuthStore()

      await authStore.getCsrfToken()
      axios.defaults.headers.common['X-XSRF-TOKEN'] = authStore.csrfToken
      axios
        .patch(`/api/${dataStore.endpoint()}/${this.endpoint}`, this.data)
        .then(() => {
          dataStore.refresh()
          this.$reset()
        })
        .catch((err) => console.log(err))
    },
    async delete() {
      const dataStore = useDataStore()
      const authStore = useAuthStore()

      await authStore.getCsrfToken()
      axios.defaults.headers.common['X-XSRF-TOKEN'] = authStore.csrfToken
      axios
        .delete(`/api/${dataStore.endpoint()}/${this.endpoint}`, this.data)
        .then(() => {
          dataStore.refresh()
          this.$reset()
        })
        .catch((err) => console.log(err))
    },
    async deletePart(endpoint) {
      const dataStore = useDataStore()
      const authStore = useAuthStore()

      await authStore.getCsrfToken()
      axios.defaults.headers.common['X-XSRF-TOKEN'] = authStore.csrfToken
      axios
        .delete(`/api/${dataStore.endpoint()}/${endpoint}`)
        .then(() => {
          dataStore.refresh()
          this.$reset()
        })
        .catch((err) => console.log(err))
    }
  }
})
