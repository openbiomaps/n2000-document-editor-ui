import { defineStore } from "pinia";
import axios from "axios";

export const useAuthStore = defineStore('auth', {
    state: () => ({
        auth: {
            email:"",
            password:""
        },
        newUser: {
            name: "",
            email: "",
            password: "",
            password_confirmation: ""
        },
        authUser: null,

        csrfToken: null,
        token: null,
        processing: false,
        error: null
    }),
    getters: {
        user: (state) => state.authUser
    },
    actions: {
        async login () {
            this.processing = true

            await this.getCsrfToken()
            
            axios.defaults.headers.common['X-XSRF-TOKEN'] = this.csrfToken;
            axios.post(`/api/login`, this.auth)
                .then((data) => {
                    this.authUser = data.data.user
                    localStorage.setItem('token', data.data.token)
                    axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`;
                    this.router.push('/')
                    this.reset()
                })
                .catch((error) => {
                    if (error.response.status == 401) {
                        this.error = error.response.data.message
                    }
                })
        },
        async register () {

            this.processing = true

            await this.getCsrfToken()
            
            axios.defaults.headers.common['X-XSRF-TOKEN'] = this.csrfToken;
            axios.post(`/api/register`, this.newUser)
                .then((data) => {
                    this.authUser = data.data.user
                    localStorage.setItem('token', data.data.token)
                    axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`;        
                    this.router.push('/')
                    this.reset()
                })
                .catch((error) => {
                    if (error.response.status == 422) {
                        this.error = error.response.data.message
                    }
                })
        },
        getCookie(name) {
            if (!document.cookie) {
              return null;
            }
          
            const xsrfCookies = document.cookie.split(';')
              .map(c => c.trim())
              .filter(c => c.startsWith(name + '='));
          
            if (xsrfCookies.length === 0) {
              return null;
            }
            return decodeURIComponent(xsrfCookies[0].split('=')[1]);
        },
        async getCsrfToken () {
            await axios.get(`/sanctum/csrf-cookie`)
            this.csrfToken = this.getCookie('XSRF-TOKEN')
        },
        async getUser () {
            axios.get(`/api/user`)
                .then((data) => this.authUser = data.data)
                .catch((error) => {
                    if (error.response.status == 401) {
                        this.router.push('/login')
                    }
                })
        },
        async logout () {
            await this.getCsrfToken()
            
            axios.defaults.headers.common['X-XSRF-TOKEN'] = this.csrfToken;
            await axios.post('/api/logout')
            this.authUser = null
            localStorage.removeItem('token')
            this.router.push('/login')
        },
        reset() {
            this.auth = { email:"", password:"" }
            this.newUser = { name: "", email: "", password: "", password_confirmation: "" },
            this.error = null
            this.processing = false
        }
    }
})
