import { defineStore } from 'pinia'
import axios from 'axios'
import Ajv from 'ajv'
import addFormats from 'ajv-formats'
import { set } from 'lodash'

export const useValidationStore = defineStore('validation', {
  state: () => ({
    data: false,
    schema: false,
    errors: {}
  }),
  actions: {
    async validateDocument() {
      if (!this.data) {
        return
      }
      console.log('validateDocument');

      const ajv = new Ajv({ allErrors: true })
      addFormats(ajv)
      ajv.addKeyword('loadSuggestions')
      const axiosdata = await axios.get(`/api/schema/celdokumentum`)
      this.schema = axiosdata.data

      const validate = ajv.compile(this.schema)
      const valid = validate(this.data)
      if (!valid) {
        this.parseErrors(validate.errors)
      }
    },
    parseErrors (errors) {      
      this.errors = {}
      errors.forEach(e => {
        set(this.errors, e.instancePath.substring(1).replaceAll("/", '.'), e)
      });
      return
    }
  }
})
